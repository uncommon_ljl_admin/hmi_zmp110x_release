﻿/* NOR Flash 分区及固件说明 */

固件名称                分区名称      分区起始地址   分区大小
u-boot.bin              UBOOT         0x0            0x40000
zmp1107-nor-env.img     ENV           0x40000        0x10000
zmp1107-nor-env.img     ENVBK         0x50000        0x10000
zmp110x_rgb_v1.0.0.dtb  DTB           0x60000        0x10000
root.romfs              ROOTFS        0x70000        0x19000
config.lfs              CONFIG        0x89000        0x100000
zImage                  KERNEL        0x189000       0x677000